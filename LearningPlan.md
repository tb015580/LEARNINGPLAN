MODELS COURSEWORK


INTRODUCTION

This is a report is on a learning plan for the Computer Science Bachelor of Science (BSc) and to show the importance of using a planned approach to run software engineering development or any other project and to help understand the similarities and crossovers of ideas in using this approach to learning.

BACKGROUND

In this report, it was importance that a planned approach was used so as to provide a wider look on the journey of learning which the computer science BSc set out for us, it was used so to avoid errors or blind studying with without looking on the grand scale. The PBS was used to first breakdown the course into years then into modules and credits while the Gantt chart showed the timeline of studying which was expected based on the credits given. Mind View 7 was an effective software to use due to its simplicity and ease of use. Gantt chart was the most useful of all options for producing a chart because of it straight forward use other software such as excel required templates with didn’t necessary fit for a learning plan.

ABSTRACTION

This is a report that simplifies the course structure for the Computer Science Bachelor of Science (BSc) course at the University of Reading using several tool and charts. The course is broken down into different modules/credits using the Product Breakdown Structure (PBS) and displayed in a Gantt chart format. The reflection delves into how using these planning methods are essential in the Computer Science (BSc) and in projects in general.

REFLECTION

By creating the Gantt chart and product breakdown structure I have been able to see how having a clear and simple structure of the timeline for projects is an indispensable advantage in the case of the computer science Bachelor of Science (BSc) course. The product breakdown structure (PBS) and Gantt chart are essential planning tools as they can make understanding and doing projects more straightforward by providing a bird’s eye view of the project for the beginning helping to plan a path of progress through projects. This also emphasises the difference in approach which has to be taken to succeed in this course whereas before in a high school setting learning involved a lot of dependence on a tutor while now learning is more of an independent task, taking the ownership of your learning is now the correct approach. 


APPENDIX

EHIJATOR AIGBOKHAEVBO
Shenfield road, Reading RG6 6HF
Ehijatora@reading.ac.uk +44 (0)7700 00000 https://www.linkedin.com/in/ehijator-aigbokhaevbo-7876a8159/
Education & Qualifications
2018-2021 University of Reading
BSc Computer Science (expected result 2:1)
•	Modules include Individual Project (TBC), high emphasis on programming, computer algorithmic process and computer architecture
2017-2018 University of Reading
International Foundation Programme (IFP)
•	Achieved the following: Further Mathematics and physics 63 (I) Statistics 67 (I) Academic skills 61 (I) Information systems 56 (II) Mathematics 45 (II)
2011-2016 Atlantic hall
IGCSE
•	ICT B MATHS C ENGLISH C CHEMISTRY C PHYSICS C BIOLOGY C

	
Work Experience

Positions of Responsibility
Started a programming club in high school
Additional Skills
IT Skills: Proficient in use of Microsoft Office applications (including Access) 
Programming: C programming
basic MATLAB skills
Languages: English
Interests & Activities
Music: Part of the school choir
Sport: First team member high school basketball team, Second team member high school football team
References available on request


